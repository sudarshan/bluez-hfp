#!/bin/sh

set -eu

PAIRADDR=""
SKIP_PAIRING="0"

while [ $# -gt 0 ] ; do
	case $1 in
	"-a") shift; PAIRADDR=$1 ;;
	"-x") set -x ;;
	"-s") SKIP_PAIRING="1" ;;
	*) usage
	esac
	shift
done

alias grep='grep -q'

OBJECT_MANAGER="gdbus call --system --dest org.bluez --object-path / --method org.freedesktop.DBus.ObjectManager"

AGENT_MANAGER="gdbus call --system --dest org.bluez --object-path /org/bluez --method org.bluez.AgentManager1"

MANAGER="gdbus call --system --dest org.bluez --object-path /org/bluez --method org.bluez.Manager1"

strip_quote() {
	echo $* | sed -e "s/.*'\(.*\)'.*/\1/"
}
	
select_adapter() {
	echo select_adapter
	ADAPTER_PATH=""
	OBJECTS=`${OBJECT_MANAGER}.GetManagedObjects`
	for WORD in ${OBJECTS} ; do
		case ${WORD} in
		"'/org/bluez/hci"[0-9]"':")
			ADAPTER_PATH=`strip_quote ${WORD}`
			break
			;;
		esac
	done

	echo "Selected ${ADAPTER_PATH}"
	ADAPTER="gdbus call --system --dest org.bluez --object-path ${ADAPTER_PATH} --method org.bluez.Adapter1"
	ADAPTER_IFACE=${ADAPTER_PATH#/org/bluez/}
        echo ${ADAPTER_IFACE}
}

select_device() {
	echo "select_device: Discovering..."
	bluez-phone-scanner &
	SCANPID=$!
	sleep 10
	kill $SCANPID

	local OBJECTS
	OBJECTS=`${OBJECT_MANAGER}.GetManagedObjects`
	for WORD in ${OBJECTS} ; do
		case ${WORD} in
		"'${ADAPTER_PATH}/dev_"*"':")
			local DEVICE_PATH
			DEVICE_PATH=`strip_quote ${WORD}`
			local DEVICEPROPERTIES
			DEVICEPROPERTIES="gdbus call --system --dest org.bluez --object-path ${DEVICE_PATH} --method org.freedesktop.DBus.Properties"
			local BDADDR
			BDADDR=`${DEVICEPROPERTIES}.Get "org.bluez.Device1" "Address"`
			BDADDR=`strip_quote ${BDADDR}`
			local BTNAME
			BTNAME=`${DEVICEPROPERTIES}.Get "org.bluez.Device1" "Name" || echo "<unnamed>"`
			BTNAME=`strip_quote ${BTNAME}`
			echo "Device found: ${BDADDR} ${BTNAME}"
			;;
		esac
	done

	echo -n "Input device address: "
	read SELECTION
	echo "Selected address: $SELECTION"

	PAIRADDR=${SELECTION}
}

select_objectpath_hfp(){
        echo select_objectpath_hfp
        local HFPPATH=""
        OBJECTPATHHFP=""
	local MANAGER
	MANAGER="gdbus call --system --dest org.ofono --object-path / --method org.ofono.Manager"
	local OBJECTPATH
        echo ${ADAPTER_PATH}
        HFPPATH="/hfp${ADAPTER_PATH}"
        echo ${HFPPATH}
	OBJECTPATH=`${MANAGER}.GetModems`
	for WORD in ${OBJECTPATH} ; do
	    case ${WORD} in 
		"'${HFPPATH}/dev_"*"',")
		  echo ${WORD}
		  OBJECTPATHHFP=`strip_quote ${WORD}`
                  echo "object path going${OBJECTPATHHFP}"
		  break
		  ;;
		esac
	done	
}

test_pairing_initiator() {
	echo test_pairing_initiator
	local BDADDR
	BDADDR=$1

	pair_device_initiator ${BDADDR}
	check_device ${BDADDR}
}

test_pairing_responder() {
	echo test_pairing_responder
	local BDADDR
	BDADDR=$1

	pair_device_responder ${BDADDR}
	check_device ${BDADDR}
}

pair_device_responder() {
	local BDADDR
	BDADDR=$1

	echo "Start a pairing from the phone ${BDADDR}! "
	pair_two hci0 ${BDADDR} -l
}

pair_device_initiator() {
	local BDADDR
	BDADDR=$1
	pair_two hci0 ${BDADDR}
}

check_device() {
	local BDADDR
	BDADDR=`echo $1 | sed "s/:/_/g"`

	local OBJECTS
	OBJECTS=`${OBJECT_MANAGER}.GetManagedObjects`
	DEVICE_PATH=""
	for WORD in ${OBJECTS} ; do
		case ${WORD} in
		"'${ADAPTER_PATH}/dev_${BDADDR}':")
			DEVICE_PATH=`strip_quote ${WORD}`
			echo "Device found: ${DEVICE_PATH}"
			;;
		esac
	done

	if [ -n "${DEVICE_PATH}" ] ; then
		DEVICE="gdbus call --system --dest org.bluez --object-path ${DEVICE_PATH} --method org.bluez.Device1"
		DEVICEPROPERTIES="gdbus call --system --dest org.bluez --object-path ${DEVICE_PATH} --method org.freedesktop.DBus.Properties"
		${DEVICEPROPERTIES}.Get "org.bluez.Device1" "Paired" | grep "(<true>,)"
		DEVICE_BDADDR=`${DEVICEPROPERTIES}.Get "org.bluez.Device1" "Address"`
		DEVICE_BDADDR=`strip_quote ${DEVICE_BDADDR}`
		echo "Device ${BDADDR} is paired"
	else
		echo "Device ${BDADDR} is not paired"
		exit 1
	fi
}

test_profile_hfp_src() {
	echo test_profile_hfp_src
        select_objectpath_hfp
        if [ -z "$OBJECTPATHHFP" ];
        then 
            echo "\$OBJECTPATHHFP is empty"
            exit 1
        else
            echo "\$OBJECTPATHHFP is not empty"
        fi    
	VCM="gdbus call --system --dest org.ofono --object-path ${OBJECTPATHHFP} --method org.ofono.VoiceCallManager"
	echo -n "Type the phone number to call:"
	read NUMBER
	echo "Dialing to the phone number: ${NUMBER}"
	echo "Calling ......."
	CALL_PATH=`${VCM}.Dial ${NUMBER} "default"`
	CALL_PATH=`strip_quote ${CALL_PATH}`
	echo "CALL_PATH ${CALL_PATH}"
	
	echo "Did you recieved the call (y/n): "
	read ANSWER
	if [ "${ANSWER}" != "y" ] ; then
		echo "HFP AG test failed"
		exit 1
	fi
	echo "Hanging up the call"
	${VCM}.HangupAll
    
        echo "From a second phone call the phone connected to oFono"
	sleep 15
	CALLS=`${VCM}.GetCalls`
        echo "${CALLS}"
	if [ "${CALLS}" != "(@a(oa{sv}) [],)" ]; then
	    echo "Call incoming"
	    VOICE_CALL="gdbus call --system --dest org.ofono --object-path ${CALL_PATH} --method org.ofono.VoiceCall"
	    ${VOICE_CALL}.Answer
	else
	   echo "Call not recieved in 15 seconds"
	fi
	
	echo "Did you hear the call (y/n): "
	read ANSWER
	if [ "${ANSWER}" != "y" ] ; then
		echo "HFP AG test failed"
		exit 1
	fi
	echo "Hanging up the call"
	${VCM}.HangupAll	
}

test_profiles() {
	echo test_profiles

	UUIDS=`${DEVICEPROPERTIES}.Get "org.bluez.Device1" "UUIDs"`
	for WORD in ${UUIDS} ; do
		case ${WORD} in
                                *"00001104-0000-1000-8000-00805f9b34fb"*) echo "IrMCSync" ;;
                                *"00001105-0000-1000-8000-00805f9b34fb"*) echo "OBEXObjectPush";;
				*"00001106-0000-1000-8000-00805f9b34fb"*) echo "OBEXFileTransfer" ;;
				*"0000110a-0000-1000-8000-00805f9b34fb"*) echo "AudioSource";;
				*"0000110b-0000-1000-8000-00805f9b34fb"*) echo "AudioSink" ;;
				*"0000110c-0000-1000-8000-00805f9b34fb"*) echo "AV Remote Control Target" ;;
				*"0000110e-0000-1000-8000-00805f9b34fb"*) echo "AV Remote Control" ;;
				*"00001112-0000-1000-8000-00805f9b34fb"*) echo "Headset Audio Gateway" ;;
				*"00001115-0000-1000-8000-00805f9b34fb"*) echo "PANU" ;;
				*"00001116-0000-1000-8000-00805f9b34fb"*) echo "NAP";;
				*"0000111e-0000-1000-8000-00805f9b34fb"*) echo "Handsfree" ;;
		                *"0000111f-0000-1000-8000-00805f9b34fb"*) echo "Handsfree Audio Gateway" ;  test_profile_hfp_src ;;
				*"0000112d-0000-1000-8000-00805f9b34fb"*) echo "SimAccess" ;;
				*"0000112f-0000-1000-8000-00805f9b34fb"*) echo "PBAP Phonebook Access PSE";;
				*"00001132-0000-1000-8000-00805f9b34fb"*) echo "MAP Message Access Server";;
				*"00001133-0000-1000-8000-00805f9b34fb"*) echo "MAP Message Notification Server" ;;
				*"00001200-0000-1000-8000-00805f9b34fb"*) echo "PnPInformation" ;;
				*"00005005-0000-1000-8000-0002ee000001"*) echo "Nokia PC Suite OBEX UUID" ;;
		*) echo "Unknown profile ${WORD}" ;;
		esac
	done
}

. common/update-test-path
select_adapter

if [ -z "${PAIRADDR}" ] ; then
	select_device
fi

if [ "${SKIP_PAIRING}" = "0" ] ; then
	test_pairing_initiator ${PAIRADDR}
	test_pairing_responder ${PAIRADDR}
else
	check_device ${PAIRADDR}
fi

test_profiles ${PAIRADDR}

trap '' EXIT
echo "PASSED"